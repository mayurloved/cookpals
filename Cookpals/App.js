/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect } from 'react';
import {
  Platform,
  StyleSheet
} from 'react-native';
import RootNavigator from './src/Components/RootNavigator';
import { userOperation } from './src/redux/Reducers/UserReducer';
import persistReducer from 'redux-persist/es/persistReducer';
import { combineReducers, createStore } from 'redux';
import persistStore from 'redux-persist/es/persistStore';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
// import KeyboardManager from 'react-native-keyboard-manager'

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
}

const rootReducer = combineReducers({
  userOperation: userOperation
});

const persistedReducer = persistReducer(persistConfig, rootReducer)

const applicationStore = createStore(persistedReducer);
const persistorStore = persistStore(applicationStore);

const App = () => {

  // useEffect(() => {
  //   if (Platform.OS === 'ios') {
  //     KeyboardManager.setEnable(true);
  //     KeyboardManager.setEnableDebugging(false);
  //     KeyboardManager.setKeyboardDistanceFromTextField(10);
  //     KeyboardManager.setLayoutIfNeededOnUpdate(true);
  //     KeyboardManager.setEnableAutoToolbar(true);
  //     KeyboardManager.setToolbarDoneBarButtonItemText("Done");
  //     KeyboardManager.setToolbarManageBehaviourBy("subviews"); // "subviews" | "tag" | "position"
  //     KeyboardManager.setToolbarPreviousNextButtonEnable(false);
  //     KeyboardManager.setToolbarTintColor('#0000FF'); // Only #000000 format is supported
  //     KeyboardManager.setToolbarBarTintColor('#FFFFFF'); // Only #000000 format is supported
  //     KeyboardManager.setShouldShowToolbarPlaceholder(true);
  //     KeyboardManager.setOverrideKeyboardAppearance(false);
  //     KeyboardManager.setKeyboardAppearance("default"); // "default" | "light" | "dark"
  //     KeyboardManager.setShouldResignOnTouchOutside(true);
  //     KeyboardManager.setShouldPlayInputClicks(true);
  //     KeyboardManager.resignFirstResponder();
  //     KeyboardManager.isKeyboardShowing()
  //       .then((isShowing) => {
  //           // ...
  //       });
  // }
  // },[])

  return (
    <Provider store={applicationStore}>
      <PersistGate loading={null} persistor={persistorStore}>
    <RootNavigator />
    </PersistGate>
    </Provider>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
