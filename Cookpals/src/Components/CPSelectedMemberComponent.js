import React from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import Assets from '../Assets';
import CPColors from '../Utils/CPColors';
import CPFonts from '../Utils/CPFonts';
import CPImageComponent from './CPImageComponent';

const CPSelectedMemberComponent = (props) => {

    return (
        <>
        <View style={props.style}>
        {props.membersArray.length !== 0 ? 
            <FlatList
                data={props.membersArray}
                numColumns={props.numColumns ?? 5}
                bounces={false}
                renderItem={({ item, index }) => {
                    return (
                        <View style={{width:(widthPercentageToDP("86.5%") / (props.numColumns ?? 5)),alignItems: 'center', marginTop: 5 }}>
                            <View style={style.memberImageView}>
                                <CPImageComponent
                                    key={index}
                                    style={style.imageStyle}
                                    source={item.image}
                                />
                            </View>
                            <CPImageComponent
                                style={{ marginTop: -10 }}
                                placeholder={Assets.close}
                            />
                            <Text style={style.nameStyle} numberOfLines={2}>{item.name}</Text>
                        </View>
                    )
                }}
            />
            : null}
            </View>
        </>
    )
};

export default CPSelectedMemberComponent;

const style = StyleSheet.create({
    memberImageView: { padding: 2, borderWidth: 1, borderColor: CPColors.imageborderColor, borderRadius: widthPercentageToDP("10%") },
    imageStyle: { width: widthPercentageToDP("10%"), height: widthPercentageToDP("10%"), borderRadius: widthPercentageToDP("10%") },
    nameStyle: { fontSize: 12, fontFamily: CPFonts.regular, color: CPColors.secondaryLight, textAlign: 'center' }
})