import React, { useState } from 'react';
import { FlatList, Pressable, StyleSheet, Text, View } from 'react-native';
import { Icon } from 'react-native-elements';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import Assets from '../Assets';
import CPColors from '../Utils/CPColors';
import CPFonts from '../Utils/CPFonts';
import CPImageComponent from './CPImageComponent';
import CPNewMemberItems from './CPNewMemberItems';
import CPSelectedMemberComponent from './CPSelectedMemberComponent';
import CPThemeButton from './CPThemeButton';

const CPNewChatItemList = (props) => {

    const [selectedIndexes, setSelectedIndexes] = useState([])

    const isShouldIndexSelected = (items) => {
        return selectedIndexes.some((item, index) => item.id == items.id)
    }

    return (
        <>
            {props.mode == 1 && selectedIndexes.length !== 0 ?
                <View style={style.container}>
                    <CPSelectedMemberComponent
                        membersArray={selectedIndexes}
                    />

                </View>
                : null}
            {props.mode == 1 && selectedIndexes.length !== 0 ? <View style={style.sepratorStyle} /> : null}
            <FlatList
                style={style.listStyle}
                data={props.chatGroupData}
                renderItem={({ item }) =>
                    <CPNewMemberItems
                        item={item}
                        isSelected={isShouldIndexSelected(item)}
                        onPress={(item) => {
                            if (isShouldIndexSelected(item)) {
                                setSelectedIndexes(selectedIndexes.filter((items, indexes) => items.id !== item.id))
                                props.onChangeReceiveMembers(selectedIndexes.filter((items, indexes) => items.id !== item.id))
                            } else {
                                setSelectedIndexes((oldArray) => [...oldArray, item])
                                props.onChangeReceiveMembers([...selectedIndexes, item])
                            }
                        }}
                    />}
            />
        </>
    );
};

export default CPNewChatItemList;

const style = StyleSheet.create({
    container: { marginHorizontal: 24, paddingVertical: 10, marginBottom:10 },
    sepratorStyle: { height: 1, borderWidth: 1, borderColor: CPColors.dropdownColor },
    listStyle:{ paddingHorizontal: 24, marginTop:5 }
})