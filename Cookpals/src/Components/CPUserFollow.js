import React, { useState } from 'react';
import { FlatList, Image, Pressable, StyleSheet, Text, View } from 'react-native';
import { useSelector } from 'react-redux';
import CPColors from '../Utils/CPColors';
import { FOLLOW, REMOVE } from '../Utils/CPConstant';
import CPFonts from '../Utils/CPFonts';
import { postApi } from '../Utils/ServiceManager';
import CPButton from './CPButton';

const CPUserInterestComponent = (props) => {

    const userSelector = useSelector((state) => state);
    const [isEnabled, setIsEnabled] = useState(false);
    const toggleSwitch = () => setIsEnabled(previousState => !previousState);

    const OnFollowChange = (item) => {
        props.setClickID(item.follower_id)
        const params = {
            status: isEnabled ? 1 : 0,
            follower_id: props?.selectedIndex === 0 ? item.user_id :item.follower_id

        }
        postApi(FOLLOW, params, onSuccessFollow, onFailureFollow, userSelector.userOperation)
       
    }

    const onSuccessFollow = (response) => {
        console.log("SUCCESS followw=>> :::::: ", response);
        if (response.success) {
            setIsEnabled(response.data.status == "0" ? true : false)
        }
    }

    const onFailureFollow = (error) => {
        console.log("FAILURE ACTIVE :::::: ", error);

    }

    const OnRemove = (item) => {
        props.setClickID(item.user_id)
        const Params = {
            follower_id: item.user_id
        }
        postApi(REMOVE, Params, onSuccessremove, onFailureRemove, userSelector.userOperation)
        console.log(item.user_id,"jajana");
    }
    const onSuccessremove = (response) => {
        console.log("SUCCESS removeeeee=>> :::::: ", response);
       
    }

    const onFailureRemove = (error) => {
        console.log("FAILURE ACTIVE :::::: ", error);
    }

    return (
        <>

            <Pressable style={styles.pressAction}
                onPress={props.onPress}
            >
                <Image
                    style={styles.postImageStyle}
                    source={{ uri: props?.selectedIndex === 0 ? props.item?.followers?.cover_image : props.item?.following?.cover_image }}
                />

                <View style={styles.detailView}>
                    <Image
                        style={styles.userImageView}
                        source={{ uri: props?.selectedIndex === 0 ? props.item?.followers?.profile : props.item?.following?.profile }}
                    />
                    <View style={styles.subDetailView}>
                        <View style={styles.userDetailViewStyle}>
                            <Text style={styles.nameStyle}>{props?.selectedIndex === 0 ? props.item?.followers?.name : props.item?.following?.name}</Text>

                            {/* {props?.selectedIndex === 0 && <Text style={styles.descriptionStyle}>{props.Country[props.index] ?? props.item.description}</Text>}
                            {props?.selectedIndex === 1 && <Text style={styles.descriptionStyle}>{[props?.Cuisine[props?.index]]?.join(',')}</Text>}
                            {props?.selectedIndex === 2 && <Text style={styles.descriptionStyle}>{[props?.Meal[props?.index]]?.join(',')}</Text>} */}

                        </View>
                        {props.isFollowing ?
                            <View style={{ alignItems: 'center' }}>
                                {props.isUnfollow ? null :
                                    <CPButton
                                        title={'Follow'}
                                        style={{ borderWidth: 0, marginTop: 5 }}
                                        textStyle={{ color: CPColors.primary }}
                                        onPress={() => {
                                            // OnFollowChange(props.item)
                                            // toggleSwitch()
                                            console.log(props.item.user_id,"lslslslmcmikm ");
                                        }}
                                    />
                                }


                                <CPButton
                                    title={props?.selectedIndex === 0 ? 'Remove' : !isEnabled ?  'Unfollow': 'Follow'}
                                    style={{ marginTop: props.isUnfollow ? 10 : 0 }}
                                    onPress={() => {
                                        OnFollowChange(props.item)
                                        toggleSwitch()
                                        console.log(props.item,'<AAAAAHHHHHH');
                                        // OnRemove(props.item)
                                    }}
                                />
                            </View>
                            : props.isFollow ?
                                <CPButton
                                    style={styles.btnStyle}
                                    title={'Follow'}
                                   
                                />
                                :
                                null}
                    </View>

                </View>
            </Pressable>

        </>
    );
};

export default CPUserInterestComponent;

const styles = StyleSheet.create({
    pressAction: { paddingBottom: 20, backgroundColor: CPColors.white },
    postImageStyle: { flex: 1, height: 90, borderRadius: 5 },
    detailView: { flexDirection: 'row', marginLeft: 15 },
    userImageView: { width: 60, height: 60, borderRadius: 60, borderColor: CPColors.white, marginTop: -20, borderWidth: 2 },
    subDetailView: { flex: 1, flexDirection: 'row' },
    userDetailViewStyle: { flex: 1, marginVertical: 10, marginHorizontal: 10 },
    nameStyle: { fontFamily: CPFonts.bold, fontSize: 14, color: CPColors.secondary },
    descriptionStyle: { fontFamily: CPFonts.semiBold, fontSize: 12, color: CPColors.secondaryLight },
    btnStyle: { alignSelf: 'center' }
})