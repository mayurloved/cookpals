import React from 'react';
import { Text, View, Pressable, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import CPColors from '../Utils/CPColors';
import CPFonts from '../Utils/CPFonts';
import CPButton from './CPButton';
import CPImageComponent from './CPImageComponent';

const CPNewMemberItems = (props) => {

    const onChangeFollowUnFollowStatus = () => {
        props.onChangeStatus(data)
    }

    return (
        <Pressable style={style.pressableStyle}
            onPress={() => props.onPress(props.item)}
        >
            <CPImageComponent
                style={style.imageStyle}
                source={props.item.image}
            />

            <View style={style.viewStyle}>
                <Text style={style.nameStyle}>{props.item.name}</Text>
                <Text style={style.subTitle}>{(props.item.country ?? props.item.status)}</Text>
            </View>
            {props.onChangeStatus ?
                <CPButton
                    style={{ alignSelf: 'center' }}
                    title={props.item.follow ? 'Follow' : 'Unfollow'}
                    onPress={onChangeFollowUnFollowStatus}
                />
                :
                <Icon name={props.isSelected ? 'check' : 'add'} style={{ backgroundColor: props.isSelected ? CPColors.primary : CPColors.transparent, borderRadius: 8, borderColor: CPColors.primary, borderWidth: 2 }} size={20} color={props.isSelected ? CPColors.white : CPColors.primary} />
            }
        </Pressable>
    );
};

export default CPNewMemberItems;

const style = StyleSheet.create({
    pressableStyle: { paddingVertical: 10, flexDirection: 'row' },
    imageStyle: { width: widthPercentageToDP("10%"), height: widthPercentageToDP("10%"), borderRadius: widthPercentageToDP("10%") },
    viewStyle: { flex: 1, marginHorizontal: 15, justifyContent: 'space-between' },
    nameStyle: { fontSize: 14, fontFamily: CPFonts.regular, color: CPColors.secondary },
    subTitle: { fontSize: 14, fontFamily: CPFonts.regular, color: CPColors.secondaryLight, marginTop: 5 }
})