import React from 'react';
import { Image, Pressable } from 'react-native';
import Assets from '../Assets';
import CPColors from '../Utils/CPColors';

const CPBackButton = (props) => {
    return (
        <Pressable
        style={props.style}
            onPress={props.onBackPress}
            // onPress={()=>{
            //     console.log("hkhkhkhk");
            // }}
        >
            <Image style={[{ marginHorizontal: 24, margin: 15 }, props.imageStyle]} source={Assets.backArrow} />
        </Pressable>
    );
};

export default CPBackButton;