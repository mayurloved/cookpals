
export const SAVE_USER_LOGGEDIN = "SAVE_USER_LOGGEDIN"
export function saveUserLoggedInInRedux(data){
    return{
        type: SAVE_USER_LOGGEDIN,
        isLoggedIn: data
    }
}

export const SAVE_USER_TOKEN = "SAVE_USER_TOKEN"
export function saveUserDetailInRedux(data){
    return{
        type: SAVE_USER_TOKEN,
        detail: data
    }
}