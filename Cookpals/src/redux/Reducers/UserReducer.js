import { SAVE_USER_LOGGEDIN, SAVE_USER_TOKEN } from "../Actions/User"

const InitialState = {
    isLoggedIn: false,
    detail:{}
}

export function userOperation(state = InitialState, { type, ...rest }) {
    switch (type) {
        
        case SAVE_USER_LOGGEDIN: {
            return { ...state, ...rest }
        }
        case SAVE_USER_TOKEN: {
            return { ...state, ...rest }
        }
        default:
            return state
    }
}