import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import Assets from '../Assets';
import CPThemeButton from '../Components/CPThemeButton';
import CPColors from '../Utils/CPColors';
import CPFonts from '../Utils/CPFonts';
import BaseContainer from './BaseContainer';

const LocationContainer = (props) => {
    return (
        <BaseContainer
            isNavigationDisable
        >
            <View style={styles.container}>
                <Image
                    source={Assets.locations}
                />
                <View style={styles.locationView}>
                    <Text style={styles.locationTitle}>{"Location Access"}</Text>
                    <Text style={styles.locationDescription}>
                        Lorem Ipsum is simply dummy text of the {`\n`}
                        Ipsum Is Simply Dummy Text Of The
                    </Text>
                </View>
            </View>
            <View style={styles.bottomView}>
                <CPThemeButton
                    title={'Location Permission'}
                    onPress={() => {
                        props.navigation.navigate('addfriendmodal')
                    }}
                />

                <CPThemeButton
                    title={"Don't Allow"}
                    colorArray={[CPColors.white, CPColors.white]}
                    // style={{backgroundColor:CPColors.white}}
                    labelStyle={styles.btnStyle}
                />

            </View>
        </BaseContainer>
    );
};

export default LocationContainer;

const styles = StyleSheet.create({
    container: { flex: 1, alignItems: 'center', justifyContent: 'center' },
    locationView: { marginTop: heightPercentageToDP("5%") },
    locationTitle: { fontFamily: CPFonts.bold, fontSize: 26, color: CPColors.secondary, marginBottom: 15, textAlign: 'center' },
    locationDescription: { fontSize: 12, fontFamily: CPFonts.medium, textAlign: 'center', color: CPColors.secondaryLight },
    bottomView: { paddingHorizontal: 24, paddingBottom: heightPercentageToDP("5") },
    btnStyle: { color: CPColors.secondary }
})