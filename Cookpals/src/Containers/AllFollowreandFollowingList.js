import React from 'react';
import BaseContainer from './BaseContainer';
import { SwipeListView } from 'react-native-swipe-list-view';
import { Image, Platform, Pressable, StyleSheet, Text, View } from 'react-native';
import CPUserInterestComponent from '../Components/CPUserInterestComponent';
import Assets from '../Assets';

const AllFollowreandFollowingList = (props) => {

    const arrayData = [
        {
            name: "Scrambled Eggs",
            description: 'Breakfast',
            image: 'https://hips.hearstapps.com/vidthumb/images/delish-cloud-eggs-horizontal-1536076899.jpg',
            user_image: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80'
        },
        {
            name: "Sandwich",
            description: 'Breakfast',
            image: 'https://static01.nyt.com/images/2020/03/04/dining/tr-egg-curry/merlin_169211805_227972c0-43d1-4f25-9643-9568331d8adb-articleLarge.jpg',
            user_image: 'https://wac-cdn.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg?cdnVersion=134'
        },
        {
            name: "Omlette",
            description: 'Breakfast',
            image: 'https://www.whiskaffair.com/wp-content/uploads/2020/04/Kerala-Egg-Curry-2.jpg',
            user_image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTIPDQwUMpolVfiXmHxCSWUEcDCO7RMSoKx5mKnEuRX2xyKLjhR-kCn2h0jb5oRn7EnCmk&usqp=CAU'
        },
        {
            name: "Scrambled Eggs",
            description: 'Breakfast',
            image: 'https://images-gmi-pmc.edge-generalmills.com/8dfd9c8e-1580-4508-a223-e5c15dd46d8e.jpg',
            user_image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRCe_o8_IQuNtFocDhlA6xVDAZ0CeM0fa2B3g&usqp=CAU'
        }
    ]

    const navigateToBack = () => {
        props.navigation.goBack()
    }

    const renderSearchList = ({ item, index }) => {
        return (<CPUserInterestComponent item={item}
            isFollow
            onPress={() => { props.navigation.navigate('subScription') }} />)
    }

    return (
        <BaseContainer
            onBackPress={navigateToBack}
            title={'Discover People'} >
            <View style={styles.container}>
                <SwipeListView
                    data={arrayData}
                    style={{paddingTop: Platform.OS == 'android' ? 30 : 10}}
                    renderItem={renderSearchList}
                    renderHiddenItem={(data, rowMap) => (
                        <View style={styles.itemListStyle}>
                            <Pressable>
                                <Image style={styles.imageStyle} source={Assets.delete} />
                            </Pressable>
                        </View>
                    )}
                    leftOpenValue={75}
                    rightOpenValue={-75}
                />
            </View>

        </BaseContainer>
    );
};

export default AllFollowreandFollowingList;

const styles = StyleSheet.create({
    container: { flex: 1, paddingHorizontal: 24 },
    itemListStyle: { flex: 1, borderColor: 'red', justifyContent: 'center', alignItems: 'flex-end' },
    imageStyle: { marginRight: 15 }
})