import React, { useEffect, useState } from 'react';
import {
  Image,
  Platform,
  Pressable,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import Snackbar from 'react-native-snackbar';
import { useDispatch, useSelector } from 'react-redux';
import Assets from '../Assets';
import CPThemeButton from '../Components/CPThemeButton';
import { saveUserLoggedInInRedux } from '../redux/Actions/User';
import CPColors from '../Utils/CPColors';
import { MY_PREFERENCE_API, PREPARE_TIME_API } from '../Utils/CPConstant';
import CPFonts from '../Utils/CPFonts';
import { getApi, postApi } from '../Utils/ServiceManager';
import { EDIT_FOOD_PREFERENCES } from '../Utils/CPConstant';
import BaseContainer from './BaseContainer';

const PrepareDetailsContainer = (props) => {

  const [preparetime, setPreparetime] = useState([]);
  const [selectedPreparetime, setSelectedPreparetime] = useState(props?.route?.params?.preptime ?? []);
  const [shouldVisible, setShouldVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const userSelector = useSelector((state) => state)
  const usersDispatcher = useDispatch()

  useEffect(() => {
    prepateTimeAPIAction()
  }, [])


  const postprefrencePrepTimeList = () => {
    const postParams = {
      type: 3,
      preparation_time_id: selectedPreparetime

    }
    setIsLoading(true)
    postApi(EDIT_FOOD_PREFERENCES, postParams, onSuccessPostPrefrencePrepTimeListUpdate, onFailurePostPrefrencePrepTimeListUpdate, userSelector.userOperation)
  }

  const onSuccessPostPrefrencePrepTimeListUpdate = (response) => {
    navigateToBack()
    console.log("SUCCESS update=>> :::::: ", response);
    setIsLoading(true)
    Snackbar.show({
      text: response.message,
      duration: Snackbar.LENGTH_LONG,
    });
  }

  const onFailurePostPrefrencePrepTimeListUpdate = (error) => {
    console.log("FAILURE ACTIVE :::::: ", error);

    Snackbar.show({
      text: response.message,
      duration: Snackbar.LENGTH_LONG,
    });
  }



  const prepateTimeAPIAction = () => {
    getApi(PREPARE_TIME_API, onSuccessPrepareTime, onFailurePrepareTime, userSelector.userOperation)
  }

  const onSuccessPrepareTime = (response) => {
    console.log("Prepare Success ::::::: ", response);
    if (response.success) {
      setPreparetime(response.data)
    } else {
      Snackbar.show({
        text: response.message,
        duration: Snackbar.LENGTH_LONG,
      });
    }
  }

  const onFailurePrepareTime = (error) => {
    console.log("Prepare Success ::::::: ", error);
    Snackbar.show({
      text: error.message,
      duration: Snackbar.LENGTH_LONG,
    });
  }

  const navigateToBack = () => {
    props.navigation.goBack();
  };

  const preparingDetailsAction = () => {
    if (selectedPreparetime) {
      if (props.route.params?.isCurrentUser) {
        postprefrencePrepTimeList()

      } else {
        myPreferenceActionAPI()
        // props.navigation.navigate('recipeList');
      }
    } else {
      setShouldVisible(true)
    }
  }

  const myPreferenceActionAPI = () => {
    const params = {
      country_id: props.route.params.country,
      meal_id: props.route.params.meals,
      cuisine_id: props.route.params.cuisine,
      gender_id: props.route.params.gender,
      most_followed_liked: props.route.params.most_followed_liked,
      preparation_time_id: selectedPreparetime
    }
    setIsLoading(true)
    postApi(MY_PREFERENCE_API, params, onSuccessPreference, onFailurePreference, userSelector.userOperation)
  }

  const onSuccessPreference = (response) => {
    if (response.success) {
      usersDispatcher(saveUserLoggedInInRedux(true))
      props.navigation.navigate('recipeList');
    } else {
      Snackbar.show({
        text: response.message,
        duration: Snackbar.LENGTH_LONG,
      });
    }
    setIsLoading(false)
  }

  const onFailurePreference = (error) => {
    Snackbar.show({
      text: error.message,
      duration: Snackbar.LENGTH_LONG,
    });
    setIsLoading(false)
  }

  const onSelectTime = (value) => {
    setShouldVisible(false)
    setSelectedPreparetime(value)
  }

  // console.log('preptime-index in preptime component::::::::::::::::', selectedPreparetime);

  return (
    <BaseContainer onBackPress={navigateToBack}
      title={props?.route?.params?.isEdit ? 'Choice Preferences' : ''}
      isLoading={isLoading}
    >
      <ScrollView bounces={false}>
        <View style={styles.main}>
          {!props?.route?.params?.isEdit &&
            <View style={styles.subView}>
              <Text style={styles.headingText}>{'Preparation Time'}</Text>
              <Text style={styles.bodyText}>
                Lorem Ipsum Is Simply Dummy Text Of The Ipsum Is Simply Dummy Text
                Of The
              </Text>
            </View>
          }
          <View style={styles.subEditView}>
            <Image source={Assets.prepareTime} style={styles.recipeImage} />

            <View style={styles.timeBtnView}>
              {preparetime.map((item) => {
                return (
                  <Pressable style={[styles.timeBtn, { backgroundColor: item.id == selectedPreparetime ? CPColors.secondary : CPColors.white }]} onPress={() => onSelectTime(item.id)}>
                    <Text style={[styles.timeText, { color: item.id == selectedPreparetime ? CPColors.white : CPColors.secondary }]}>{item?.time + " min"}</Text>
                  </Pressable>
                )
              })}
            </View>
          </View>
          {!selectedPreparetime && shouldVisible ? <Text style={{ fontSize: 14, color: CPColors.red, fontFamily: CPFonts.medium, marginVertical: 5 }}>{"Please select time"}</Text> : null}
        </View>
      </ScrollView>
      <CPThemeButton
        title={props.route.params.isCurrentUser ? 'Save Changes' : 'Next'}
        style={styles.nextBtn}
        isLoading={isLoading}
        onPress={preparingDetailsAction}
      />
    </BaseContainer>
  );
};

const styles = StyleSheet.create({
  main: {
    flex: 1,
    paddingHorizontal: 20,
    alignItems: 'center',
    paddingBottom: 20,
    marginTop: Platform.OS == 'android' ? 30 : 0,
  },
  subView: {
    alignItems: 'center',
    marginHorizontal: 40,
    marginVertical: 20,
  },
  subEditView: {
    alignItems: 'center',
    marginTop: 40,
  },
  headingText: {
    fontFamily: CPFonts.semiBold,
    fontSize: 18,
    marginBottom: 10,
    color: CPColors.secondary,
  },
  bodyText: {
    fontFamily: CPFonts.regular,
    textAlign: 'center',
    fontSize: 12,
    color: CPColors.secondary,
  },
  recipeImage: {
    marginVertical: 20,
  },
  timeBtnView: {
    flexDirection: 'row',
    marginHorizontal: 30,
    marginVertical: 20,
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  timeBtn: {
    padding: 6,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: CPColors.secondaryLight,
    marginBottom: 20,
    marginLeft: 10,
  },
  timeText: {
    fontSize: 14,
    fontFamily: CPFonts.semiBold,
  },
  nextBtn: {

    margin: 20,
    width: widthPercentageToDP('100') - 40,
  },
});

export default PrepareDetailsContainer;
