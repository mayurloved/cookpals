import React, { useState } from 'react';
import { Image, KeyboardAvoidingView, StyleSheet, Text, TextInput, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Snackbar from 'react-native-snackbar';
import VideoPlayers from 'react-native-video-players';
import { useSelector } from 'react-redux';
import Assets from '../Assets';
import CPProfileImage from '../Components/CPProfileImage';
import CPRecipeVideo from '../Components/CPRecipeVideo';
import CPThemeButton from '../Components/CPThemeButton';
import CPVideoPlayerComponent from '../Components/CPVideoPlayerComponent';
import CPColors from '../Utils/CPColors';
import { POST_ADD_API } from '../Utils/CPConstant';
import { POST_EDIT_API } from '../Utils/CPConstant';
import CPFonts from '../Utils/CPFonts';
import { postApi } from '../Utils/ServiceManager';
import ValidationHelper from '../Utils/ValidationHelper';
import BaseContainer from './BaseContainer';


// var videoDetails = null
const AddFunnyPostContainer = (props) => {

    const userSelector = useSelector((state) => state)
    const [description, setDescription] = useState(props?.route?.params?.description ?? "");
    const [id, setId] = useState(props?.route?.params?.id ?? "")
    const [shouldVisible, setShouldVisible] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [videoDetails, setVideoDetails] = useState(props?.route?.params?.video ?? null);
    const validationHelper = new ValidationHelper();

    console.log(id, ':::::id of the funny video in the edit container');

    const navigateToBack = () => {
        props.navigation.goBack()
    }

    const onAddFunnyVideo = () => {
        setShouldVisible(true)
        if (description.trim() == "" ||
            videoDetails == null
        ) {
            return
        } else {
            props?.route?.params?.isEdit ? editFunnyVideoAction() : addFunnyVideoAction()
        }

    }

    console.log(videoDetails, 'VIDEO URL');

    const editFunnyVideoAction = () => {
        const params = {
            type: 2,
            description: description,

        }
        if (videoDetails?.uri) {
            params['video'] = videoDetails
        }
        setShouldVisible(false)
        setIsLoading(true)
        postApi(POST_EDIT_API + id, params, onSuccessEditFunnyPost, onFailureEditFunnyPost, userSelector.userOperation)
    }

    const onSuccessEditFunnyPost = (response) => {
        console.log("ADD Funny Post ::::: ", response);
        if (response.success) {
            // setDescription(description)
            // setVideoDetails(videoDetails)
            props.navigation.goBack()
        }
        setIsLoading(false)
        Snackbar.show({
            text: response.message,
            duration: Snackbar.LENGTH_LONG,
        });
    }

    const onFailureEditFunnyPost = (error) => {
        console.log("FAILURE Funny Post ::::: ", error);
        setIsLoading(false)
        Snackbar.show({
            text: error.message,
            duration: Snackbar.LENGTH_LONG,
        });
    }





    const addFunnyVideoAction = () => {
        const params = {
            type: 2,
            description: description,
            video: videoDetails
        }
        setShouldVisible(false)
        setIsLoading(true)
        postApi(POST_ADD_API, params, onSuccessAddFunnyPost, onFailureAddFunnyPost, userSelector.userOperation)
    }

    const onSuccessAddFunnyPost = (response) => {
        console.log("ADD Funny Post ::::: ", response);
        if (response.success) {
            setDescription("")
            setVideoDetails(null)
            props.navigation.goBack()

        }
        setIsLoading(false)
        Snackbar.show({
            text: response.message,
            duration: Snackbar.LENGTH_LONG,
        });
    }

    const onFailureAddFunnyPost = (error) => {
        console.log("FAILURE Funny Post ::::: ", error);
        setIsLoading(false)
        Snackbar.show({
            text: error.message,
            duration: Snackbar.LENGTH_LONG,
        });
    }

    const onHandleVideo = (data) => {
        if (props?.route?.params?.isEdit) {
            setVideoDetails(data.uri)
        } else {

            setVideoDetails(data)
            setShouldVisible(false)
        }
    }

    const onChangeDescription = (value) => {
        setShouldVisible(false)
        setDescription(value)
    }

    return (
        <BaseContainer
            title={props?.route?.params?.isEdit ? 'Edit Funny Video' : 'Post Funny Video'}
            onBackPress={navigateToBack}
            isLoading={isLoading}
        >
            <View style={{ flex: 1, paddingBottom: 10 }}>
                <KeyboardAwareScrollView>


                    <CPVideoPlayerComponent
                        source={props?.route?.params?.isEdit? videoDetails: videoDetails?.uri}
                        onSelectImageData={onHandleVideo}
                        playerStyle={{
                            height: 150,
                            width: '85%',
                            alignSelf: 'center',
                            marginVertical: 20
                        }}
                        imageStyle={style.stepImageStyle}
                    />
                    {shouldVisible && videoDetails == null ?
                        <Text style={{ color: CPColors.red, fontSize: 12, fontFamily: CPFonts.regular, marginTop: 5, marginLeft: 5, textAlign: "center", marginBottom: 10 }}>{"Please add video"}</Text>
                        : null}

                    <TextInput
                        value={description}
                        style={{
                            height: 150,
                            borderRadius: 10,
                            borderWidth: 0.4,
                            borderColor: CPColors.borderColor,
                            marginHorizontal: 24,
                            paddingHorizontal: 15,
                            paddingTop: 15,
                            color: CPColors.secondary,
                            fontSize: 16,
                            fontFamily: CPFonts.regular
                        }}
                        placeholder={'Description...'}
                        placeholderTextColor={CPColors.secondaryLight}
                        onChangeText={onChangeDescription}
                        multiline
                    />
                    {shouldVisible && validationHelper.isEmptyValidation(description, "Please add description").trim() !== "" ?
                        <Text style={{ color: CPColors.red, fontSize: 12, fontFamily: CPFonts.regular, marginTop: 5, marginLeft: 24, marginTop: 10 }}>{"Please add description"}</Text>
                        : null}

                </KeyboardAwareScrollView>

                <CPThemeButton
                    title={props?.route?.params?.isEdit ? 'Save' : 'Post'}
                    onPress={onAddFunnyVideo}
                    isLoading={isLoading}
                />
            </View>
        </BaseContainer>
    );
};

export default AddFunnyPostContainer;

const style = StyleSheet.create({
    stepImageStyle: { marginVertical: 50, alignSelf: 'center', borderWidth: 1 }
})