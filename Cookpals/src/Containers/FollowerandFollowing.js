import { useFocusEffect } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { FlatList, Image, Platform, Pressable, StyleSheet, Text, View } from 'react-native';
import { useSelector } from 'react-redux';
import Assets from '../Assets';
import CPSearchComponent from '../Components/CPSearchComponent';
import CPSegmentComponent from '../Components/CPSegmentComponent';
import CPUserFollow from '../Components/CPUserFollow';
import CPColors from '../Utils/CPColors';
import { FOLLOWING } from '../Utils/CPConstant';
import CPFonts from '../Utils/CPFonts';
import { getApi } from '../Utils/ServiceManager';
import BaseContainer from './BaseContainer';

const FollowerandFollowing = (props) => {

    const userSelector = useSelector((state) => state);
    const followersArray = ["Followers", "Following"]
    const [selectedIndex, setIndex] = useState(0)
    const [follower, setFollower] = useState()
    const [following, setFollowing] = useState()
    const [clickID, setClickID] = useState()

    // useEffect(() => {
    //     followList()
    // }, [])

    useFocusEffect(
        React.useCallback(() => {
            followList()
        }, []),
      );

    const followList = () => {
        getApi(FOLLOWING, onSuccesFollowing, onFailureFollowing, userSelector.userOperation)
    }
    
    const onSuccesFollowing = (response) => {
        
        if (response.success) {
            setFollower(response?.data?.followers)
            setFollowing(response?.data?.following)
        }
    }
    console.log(follower,"followoowww");
    
    const onFailureFollowing = (error) => {
       
    }

    const onChangeHandler = (value) => {
        setIndex(value)
    }

    const navigateToBack = () => {
        props.navigation.goBack()
    }

    const renderSearchList = ({ item, index }) => {
        return (
        <CPUserFollow 
            item={item}
            index={index}
            isFollowing
            selectedIndex={selectedIndex}
            setClickID={setClickID}
            isUnfollow={selectedIndex}
            onPress={() => {
                props.navigation.navigate('anotherUser', {
                    isAnotherUser: true,
                    fromClick: true,
                    id: selectedIndex== 0? item.user_id : item.follower_id
                })
            }} />)
        }
       
    return (
        <BaseContainer
            titleComponent={true}
            onBackPress={navigateToBack}
            leftcmpStyle={{ flex: 0 }}
            rightComponent={
                <CPSearchComponent
                    placeholder={'Search by name,cusine or meal type...'}
                    style={styles.searchStyle}
                />
            }
        >
            <View style={styles.container}>

                <CPSegmentComponent
                    segmentArray={followersArray}
                    selectedIndex={selectedIndex}
                    onChangeHandler={onChangeHandler}
                // style={{marginTop:30}}
                />

                <FlatList data={selectedIndex== 0? follower : following} contentContainerStyle={styles.listView}
                    ListFooterComponent={() => {
                        return (
                            <>
                                {selectedIndex ? null :
                                    <Pressable style={styles.listViewPress}
                                        onPress={() => {
                                            props.navigation.navigate('allfollowandfollowing')
                                        }}
                                    >
                                        <Text style={styles.footerText}>{"See All Suggestions"}</Text>
                                    </Pressable>
                                }
                            </>
                        )
                    }}
                    renderItem={renderSearchList} />

            </View>
        </BaseContainer>
    );
};

export default FollowerandFollowing;

const styles = StyleSheet.create({
    searchStyle: {
        flex: 1,
        marginBottom: -30,
        marginRight: 20
    },
    container: { flex: 1, paddingHorizontal: 24, marginTop: Platform.OS == 'android' ? 50 : 30 },
    listView: { paddingVertical: 20 },
    listViewPress: { alignSelf: 'flex-start' },
    footerText: { fontSize: 14, fontFamily: CPFonts.medium, color: CPColors.primary }
})