import React, { useState } from 'react';
import { Image, Platform, Pressable, StyleSheet, View } from 'react-native';
import { hasNotch } from 'react-native-device-info';
import { SwipeListView } from 'react-native-swipe-list-view';
import Assets from '../Assets';
import CPChatItemList from '../Components/CPChatItemList';
import CPSegmentComponent from '../Components/CPSegmentComponent';
import BaseContainer from './BaseContainer';

const ChatListContainer = (props) => {

    const chatArray = ["person", "Group"]
    const [selectedIndex, setIndex] = useState(0)

    const chatListArray = [
        {
            name: "James Jackson",
            message: "Hi there how are you?",
            image: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80',
            count: 2
        },
        {
            name: "James Jackson",
            message: "Hi there how are you?",
            image: 'https://wac-cdn.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg?cdnVersion=134',
            count: 0
        },
        {
            name: "James Jackson",
            message: "Hi there how are you?",
            image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTIPDQwUMpolVfiXmHxCSWUEcDCO7RMSoKx5mKnEuRX2xyKLjhR-kCn2h0jb5oRn7EnCmk&usqp=CAU',
            count: 2
        },
        {
            name: "James Jackson",
            message: "Hi there how are you?",
            image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRCe_o8_IQuNtFocDhlA6xVDAZ0CeM0fa2B3g&usqp=CAU',
            count: 2
        },
        {
            name: "James Jackson",
            message: "Hi there how are you?",
            image: 'https://wac-cdn.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg?cdnVersion=134',
            count: 0
        }
    ]

    const onChangeHandler = (value) => {
        setIndex(value)
    }

    const navigateToBack = () => {
        props.navigation.goBack()
    }
    console.log("HAS ", hasNotch());

    return (
        <BaseContainer
            title={"Chat"}
            onBackPress={navigateToBack}
            titleStyle={styles.flexStyle}
            rightComponent={
                <View style={styles.flexStyle}>
                    <Pressable style={styles.groupPress}
                        onPress={() => {
                            props.navigation.navigate('newchatlist', { selectMode: selectedIndex })
                        }}
                    >
                        <Image
                            source={Assets.addgroup}
                        />
                    </Pressable>
                </View>
            }
        >
            <View style={styles.flexStyle}>
                <CPSegmentComponent
                    style={styles.segmentStyle}
                    segmentArray={chatArray}
                    selectedIndex={selectedIndex}
                    onChangeHandler={onChangeHandler}
                />

                <CPChatItemList chatListData={chatListArray}
                    onPress={(data) => {
                        props.navigation.navigate('chat', {
                            userData: data,
                            selectMode: selectedIndex
                        })
                    }}
                />

            </View>
        </BaseContainer>
    );
};

export default ChatListContainer;

const styles = StyleSheet.create({
    flexStyle: { flex: 1 },
    groupPress: { alignSelf: 'flex-end', marginRight: 20 },
    segmentStyle: { paddingHorizontal: 24 }
})