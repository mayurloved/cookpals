import React, { useRef, useState } from 'react';
import { Platform, ScrollView, SectionList, StyleSheet } from 'react-native';
import { FlatList, Pressable, Text, View } from 'react-native';
import { Icon } from 'react-native-elements';
import CPImageComponent from '../Components/CPImageComponent';
import CPNewMemberItems from '../Components/CPNewMemberItems';
import CPSelectedMemberComponent from '../Components/CPSelectedMemberComponent';
import CPThemeButton from '../Components/CPThemeButton';
import CPColors from '../Utils/CPColors';
import CPFonts from '../Utils/CPFonts';
import BaseContainer from './BaseContainer';

const AddFriendContainer = (props) => {

    const scrollIndicatorRef = useRef();
    const sectionRef = useRef();

    const segmentArray = [
        {
            title: 'Contacts'
        },
        {
            title: 'Facebook Friends'
        },
        {
            title: 'Instagram Friends'
        }
    ]

    const selectedMember = [
        {
            name: "James Jackson",
            message: "Hi there how are you?",
            image: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80',
            count: 2
        },
        {
            name: "James Jackson",
            message: "Hi there how are you?",
            image: 'https://wac-cdn.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg?cdnVersion=134',
            count: 0
        },
        {
            name: "James Jackson",
            message: "Hi there how are you?",
            image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTIPDQwUMpolVfiXmHxCSWUEcDCO7RMSoKx5mKnEuRX2xyKLjhR-kCn2h0jb5oRn7EnCmk&usqp=CAU',
            count: 2
        },
        {
            name: "James Jackson",
            message: "Hi there how are you?",
            image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRCe_o8_IQuNtFocDhlA6xVDAZ0CeM0fa2B3g&usqp=CAU',
            count: 2
        },
        {
            name: "James Jackson",
            message: "Hi there how are you?",
            image: 'https://wac-cdn.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg?cdnVersion=134',
            count: 0
        }
    ]

    const cookpalsMember = [
        {
            name: "Patrick Osborne",
            status: "You’re friends on Facebook",
            image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTIPDQwUMpolVfiXmHxCSWUEcDCO7RMSoKx5mKnEuRX2xyKLjhR-kCn2h0jb5oRn7EnCmk&usqp=CAU'
        },
        {
            name: "Caleb Klein",
            status: "You’re friends on Facebook",
            image: 'https://wac-cdn.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg?cdnVersion=134'
        },
        {
            name: "Lois Conner",
            status: "You’re friends on Facebook",
            image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTIPDQwUMpolVfiXmHxCSWUEcDCO7RMSoKx5mKnEuRX2xyKLjhR-kCn2h0jb5oRn7EnCmk&usqp=CAU'
        },
        {
            name: "Jerome Gonzales",
            status: "You’re friends on Facebook",
            image: 'https://wac-cdn.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg?cdnVersion=134'
        }
    ]

    const allMembers = [
        {
            title: "A",
            data: [
                {
                    name: "Adith Owen",
                    status: "You’re friends on Facebook",
                    image: 'https://wac-cdn.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg?cdnVersion=134',
                },
                {
                    name: "Adam Oley",
                    status: "You’re friends on Facebook",
                    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRCe_o8_IQuNtFocDhlA6xVDAZ0CeM0fa2B3g&usqp=CAU'
                }
            ]
        },
        {
            title: "B",
            data: [
                {
                    name: "Brain antony",
                    status: "You’re friends on Facebook",
                    image: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80'
                },
                {
                    name: "Brown deo",
                    status: "You’re friends on Facebook",
                    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTIPDQwUMpolVfiXmHxCSWUEcDCO7RMSoKx5mKnEuRX2xyKLjhR-kCn2h0jb5oRn7EnCmk&usqp=CAU'
                }
            ]
        },
        {
            title: "C",
            data: [
                {
                    name: "Carlo brahwate",
                    status: "You’re friends on Facebook",
                    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTIPDQwUMpolVfiXmHxCSWUEcDCO7RMSoKx5mKnEuRX2xyKLjhR-kCn2h0jb5oRn7EnCmk&usqp=CAU'
                },
                {
                    name: "Chris gayle",
                    status: "You’re friends on Facebook",
                    image: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80'
                }
            ]
        },
        {
            title: "D",
            data: [
                {
                    name: "Deno moria",
                    status: "You’re friends on Facebook",
                    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTIPDQwUMpolVfiXmHxCSWUEcDCO7RMSoKx5mKnEuRX2xyKLjhR-kCn2h0jb5oRn7EnCmk&usqp=CAU'
                },
                {
                    name: "Denny Denzappa",
                    status: "You’re friends on Facebook",
                    image: 'https://wac-cdn.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg?cdnVersion=134',
                }
            ]
        },
        {
            title: "E",
            data: [
                {
                    name: "Edam Christen",
                    status: "You’re friends on Facebook",
                    image: 'https://wac-cdn.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg?cdnVersion=134'
                },
                {
                    name: "Ena Andrew",
                    status: "You’re friends on Facebook",
                    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTIPDQwUMpolVfiXmHxCSWUEcDCO7RMSoKx5mKnEuRX2xyKLjhR-kCn2h0jb5oRn7EnCmk&usqp=CAU'
                }
            ]
        },
        {
            title: "F",
            data: [
                {
                    name: "Faf Plesy",
                    status: "You’re friends on Facebook",
                    image: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80'
                },
                {
                    name: "Forman Andrew",
                    status: "You’re friends on Facebook",
                    image: 'https://wac-cdn.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg?cdnVersion=134',
                }
            ]
        },
        {
            title: "G",
            data: []
        },
        {
            title: "H",
            data: []
        },
        {
            title: "I",
            data: [
                {
                    name: "Ian bothom",
                    status: "You’re friends on Facebook",
                    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTIPDQwUMpolVfiXmHxCSWUEcDCO7RMSoKx5mKnEuRX2xyKLjhR-kCn2h0jb5oRn7EnCmk&usqp=CAU'
                },
                {
                    name: "Iliana warn",
                    status: "You’re friends on Facebook",
                    image: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80'
                }
            ]
        },
        {
            title: "J",
            data: []
        },
        {
            title: "K",
            data: [
                {
                    name: "Kay petersen",
                    status: "You’re friends on Facebook",
                    image: 'https://wac-cdn.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg?cdnVersion=134'
                },
                {
                    name: "Kaine williamson",
                    status: "You’re friends on Facebook",
                    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTIPDQwUMpolVfiXmHxCSWUEcDCO7RMSoKx5mKnEuRX2xyKLjhR-kCn2h0jb5oRn7EnCmk&usqp=CAU'
                }
            ]
        },
        {
            title: "L",
            data: []
        },
        {
            title: "M",
            data: []
        },
        {
            title: "N",
            data: []
        },
        {
            title: "O",
            data: []
        },
        {
            title: "P",
            data: []
        },
        {
            title: "Q",
            data: []
        },
        {
            title: "R",
            data: []
        },
        {
            title: "S",
            data: []
        },
        {
            title: "T",
            data: []
        },
        {
            title: "U",
            data: []
        },
        {
            title: "V",
            data: []
        },
        {
            title: "W",
            data: [
                {
                    name: "Wels deno",
                    status: "You’re friends on Facebook",
                    image: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80'
                },
                {
                    name: "William george",
                    status: "You’re friends on Facebook",
                    image: 'https://wac-cdn.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg?cdnVersion=134'
                }
            ]
        },
        {
            title: "X",
            data: []
        },
        {
            title: "Y",
            data: []
        },
        {
            title: "Z",
            data: []
        }
    ]
    const alphabets = ['#', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

    const [selectedIndex, setSelectedIndex] = useState(0)

    const onBackPress = () => {
        props.navigation.goBack()
    }

    const contactSegmentComponent = () => {
        return (
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 24, backgroundColor: CPColors.dropdownColor, marginVertical: 15, borderRadius: 15 }}>
                {segmentArray.map((item, index) => {
                    return (
                        <Pressable style={{ alignItems: 'center', justifyContent: 'center', borderBottomColor: selectedIndex == index ? CPColors.primary : CPColors.transparent, borderBottomWidth: 1.5 }}
                            onPress={() => setSelectedIndex(index)}
                        >
                            <Text style={{ fontSize: 12, marginVertical: 10, fontFamily: CPFonts.medium, color: selectedIndex == index ? CPColors.secondary : CPColors.secondaryLight }}>{item.title}</Text>
                        </Pressable>
                    )
                })}
            </View>
        )
    }

    const headerRender = () => {
        return (
            <>
                <Text style={styles.headerTitleStyle}>{"Invite Friends On Cookpals"}</Text>
                <FlatList
                    data={cookpalsMember}
                    scrollEnabled={false}

                    renderItem={({ item, index }) => {
                        return (
                            <View style={styles.headerListView}>
                                <CPImageComponent
                                    source={item.image}
                                    style={styles.headerImage}
                                />

                                <View style={styles.headerListSubView}>
                                    <Text style={styles.headerNameStyle}>{item.name}</Text>
                                    <Text style={styles.headerStatusStyle}>{item.status}</Text>
                                </View>
                            </View>
                        )
                    }}
                    ItemSeparatorComponent={() => {
                        return (
                            <View
                                style={styles.headerSeprator}
                            />
                        )
                    }}
                />
                <Text style={styles.headerBottomTxt}>{"Add More Friends On Cookpals"}</Text>
            </>
        )
    }

    const sectionHeaderRender = ({ section: { title } }) => {
        return (
            <View style={styles.sectionHeaderStyle}>
                <Text style={styles.sectionHeaderTxtStyle}>{title}</Text>
            </View>
        )
    }

    
    const scrollIndicatorRender = () => {
        return (
            <View style={styles.scrollIndicatorViewStyle}>
                <FlatList
                    ref={scrollIndicatorRef}
                    data={alphabets}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item, index }) => {
                        return (
                            <Pressable
                                onPress={() => {
                                    if (index !== 0) {
                                        sectionRef.current.scrollToLocation(
                                            {
                                                sectionIndex: index - 1,
                                                itemIndex: 0
                                            }
                                        )
                                    } else {
                                        // sectionRef.current.scrollToLocation({
                                        //     animated: true,
                                        //     sectionIndex: 0,
                                        //     itemIndex: 0,
                                        //     viewPosition: 0
                                        //   });
                                    }
                                }}
                            >
                                <Text style={styles.indicatorItemlist}>{item}</Text>
                            </Pressable>
                        )
                    }}
                />
                {/* <ScrollView contentContainerStyle={{ paddingVertical: 10 }} showsVerticalScrollIndicator={false}>
                    {alphabets.map((item, index) => { 
                        return (
                            <Pressable
                                onPress={() => { }}
                            >
                                <Text style={styles.indicatorItemlist}>{item}</Text>
                            </Pressable>
                        )
                     })}
                </ScrollView> */}
            </View>
        )
    }

    return (
        <BaseContainer
            title={"Add Friends"}
            rightComponent={
                <View style={styles.rightComponentStyle}>
                    <Pressable style={styles.rightComponentPress}>
                        <Icon
                            type={'material-icons'}
                            name={'search'}
                            color={CPColors.secondary}
                            style={{ margin: 5 }}
                        />
                    </Pressable>
                </View>
            }
            onBackPress={onBackPress}
        >

            <View style={{ flex: 1 }}>
                <Text style={styles.titleStyle}>{"Friends"}</Text>
                <Text style={styles.subTitle}>{"Lorem ipsum dolor sit amet"}</Text>

                {contactSegmentComponent()}

                <View style={styles.container}>
                    <CPSelectedMemberComponent
                        membersArray={selectedMember}
                    />
                    <View style={styles.subContainer}>

                        <SectionList
                            ref={sectionRef}
                            sections={allMembers}
                            showsVerticalScrollIndicator={false}
                            ListHeaderComponent={headerRender}
                            renderSectionHeader={sectionHeaderRender}
                            renderItem={({ item, index }) => {
                                return (
                                    <CPNewMemberItems item={item} />
                                )
                            }}
                        />
                        {scrollIndicatorRender()}
                    </View>


                </View>

                <CPThemeButton
                    style={{ margin: 15 }}
                    title={'Next'}
                    onPress={() => {
                        props.navigation.navigate('foodPreference')
                    }}
                />

            </View>
        </BaseContainer>
    );
};

export default AddFriendContainer;

const styles = StyleSheet.create({
    titleStyle: { fontFamily: CPFonts.semiBold, fontSize: 18, color: CPColors.secondary, marginHorizontal: 24, marginTop: 15, marginBottom: 10 },
    subTitle: { fontFamily: CPFonts.regular, fontSize: 12, color: CPColors.secondaryLight, marginHorizontal: 24 },
    container: { flex: 1, paddingLeft: 18, paddingRight: 6, backgroundColor: CPColors.dropdownColor, borderTopRightRadius: 20, borderTopLeftRadius: 20 },
    subContainer: { flex: 1, paddingLeft: 10, marginTop: 10, flexDirection: 'row' },
    headerTitleStyle: { marginTop: 15, marginBottom: 5, fontSize: 14, fontFamily: CPFonts.semiBold, color: CPColors.primary },
    headerListView: { flexDirection: 'row', paddingVertical: 20 },
    headerImage: { width: 40, height: 40, borderRadius: 40 },
    headerListSubView: { marginHorizontal: 15, justifyContent: 'space-between' },
    headerNameStyle: { fontFamily: CPFonts.regular, fontSize: 14, color: CPColors.secondary },
    headerStatusStyle: { fontFamily: CPFonts.regular, fontSize: 14, color: CPColors.secondaryLight },
    headerSeprator: { height: 0.8, backgroundColor: CPColors.textInputColor },
    headerBottomTxt: { fontFamily: CPFonts.semiBold, fontSize: 14, color: CPColors.primary },
    sectionHeaderStyle: { backgroundColor: CPColors.dropdownColor },
    sectionHeaderTxtStyle: { fontSize: 20, fontFamily: CPFonts.medium, color: CPColors.secondary, marginVertical: 10 },
    scrollIndicatorViewStyle: { marginLeft: 10, marginTop: 25 },
    indicatorItemlist: { textAlign: 'center', fontSize: 12, fontFamily: CPFonts.medium, color: CPColors.borderColor, marginVertical: 5 },
    rightComponentStyle: { flex: 1, alignItems: 'center' },
    rightComponentPress: { backgroundColor: CPColors.dropdownColor, borderRadius: 10 }
})