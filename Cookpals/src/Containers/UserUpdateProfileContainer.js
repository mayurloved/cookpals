import React, { useEffect, useState } from 'react';
import { Pressable, ScrollView, StyleSheet, Text } from 'react-native';
import { Image, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Assets from '../Assets';
import CPBackButton from '../Components/CPBackButton';
import CPImageComponent from '../Components/CPImageComponent';
import CPProfileImage from '../Components/CPProfileImage';
import CPTextInput from '../Components/CPTextInput';
import CPThemeButton from '../Components/CPThemeButton';
import CPColors from '../Utils/CPColors';
import CPDatePickerComponent from '../Components/CPDatePickerComponent';
import CPGenderSelectionComponent from '../Components/CPGenderSelectionComponent';
import ValidationHelper from '../Utils/ValidationHelper';
import CPProfileComponent from '../Components/CPProfileComponent';
import { useDispatch, useSelector } from 'react-redux';
import { getApi, postApi } from '../Utils/ServiceManager';
import { GET_GENDER_API, UPDATE_PROFILE_API } from '../Utils/CPConstant';
import { saveUserDetailInRedux } from '../redux/Actions/User';
import Snackbar from 'react-native-snackbar';
import CPFonts from '../Utils/CPFonts';
import { Icon } from 'react-native-elements';

var coverImageDetail = null
var userProfileImageDetail = null
const UserUpdateProfileContainer = (props) => {

    const usersDispatcher = useDispatch();
    const userSelector = useSelector((state) => state)
    const [fullName, setfullName] = useState(userSelector?.userOperation?.detail.name ?? "");
    const [phoneNumber, setphoneNumber] = useState(userSelector?.userOperation?.detail.phone_number ?? "");
    const [description, setDescription] = useState(userSelector?.userOperation?.detail.bio ?? "");
    const [gender, setGender] = useState(userSelector?.userOperation?.detail.gender ?? "");
    const [genderArray, setGenderArray] = useState([]);
    const [shouldVisible, setShouldVisible] = useState(false);
    const [lgbtq, setlgbtq] = useState(userSelector?.userOperation?.detail.lgbtq == 1 ? true : false);
    const [isLoading, setIsLoading] = useState(false);
    const [isTermCondition, setTermCondition] = useState(false);

    // var coverImageDetail = null
    // var userProfileImageDetail = null

    const validationHelper = new ValidationHelper();

    useEffect(() => {
        genderReceiveAction()
    }, [])

    const genderReceiveAction = () => {
        getApi(GET_GENDER_API, onSuccessGetGender, onFailureGetGender)
    }

    const onSuccessGetGender = (response) => {
        console.log(" :::::::: ", response);
        setGenderArray(response.data.filter((item, index) => index !== 2))
    }

    const onFailureGetGender = (error) => {
        console.log("ERROR :::::::: ", error);
    }

    const navigateToBack = () => {
        props.navigation.goBack()
    }

    const onChangeFullName = (text) => {
        setShouldVisible(false)
        setfullName(text)
    }

    const onChangeDescription = (text) => {
        setShouldVisible(false)
        setDescription(text)
    }

    const onChangePhoneNumber = (text) => {
        setShouldVisible(false)
        setphoneNumber(text)
    }

    const updateProfile = () => {
        const params = {
            name: fullName,
            gender: gender,
            phone_number: phoneNumber,
            bio: description,
            lgbtq: lgbtq ? 1 : 0,
        }
        if (coverImageDetail) {
            params['cover_image'] = coverImageDetail
        }

        if (userProfileImageDetail) {
            params['profile'] = userProfileImageDetail
        }
        setIsLoading(true)
        postApi(UPDATE_PROFILE_API, params, onSuccessUpdateProfile, onFailureUpdateProfile, userSelector.userOperation)
    }

    const onSuccessUpdateProfile = (response) => {
        console.log("SUCCESS UPDATE ::::: ", response);
        if (response.success) {
            let dict = response.data
            dict['token'] = userSelector.userOperation.detail.token
            usersDispatcher(saveUserDetailInRedux(dict))
            props.navigation.goBack()
        } else {
            Snackbar.show({
                text: response.message,
                duration: Snackbar.LENGTH_LONG,
            });
        }
        setIsLoading(false)
    }

    const onFailureUpdateProfile = (error) => {
        console.log("Failure ::::: ", error);
        setIsLoading(false)
        Snackbar.show({
            text: error.message,
            duration: Snackbar.LENGTH_LONG,
        });
    }

    const userDetailValidation = () => {
        setShouldVisible(true)
        if (
            fullName.trim() == "" ||
            phoneNumber.trim() == "" ||
            description.trim() == "" ||
            !gender ||
            (!props?.route?.params?.isCurrentUser && coverImageDetail == null)
        ) {
            return
        } else {
            if (props?.route?.params?.isCurrentUser) {
                updateProfile()
            } else {
                let params = {
                    email: props.route?.params?.email,
                    date_of_birth: props.route?.params?.dob,
                    password: props.route?.params?.password,
                    confirm_password: props.route?.params?.password,
                    name: fullName,
                    phone_number: phoneNumber,
                    bio: description,
                    lgbtq: lgbtq ? 1 : 0,
                    gender: gender
                }

                console.log("IMageeee ::::: ", coverImageDetail);
                if (coverImageDetail) {
                    params.cover_image = coverImageDetail
                }

                if (userProfileImageDetail) {
                    params.profile = userProfileImageDetail
                }
                props.navigation.navigate('otp', params)
            }
        }
    }

    return (
        <View style={styles.flexView} pointerEvents={isLoading ? 'none' : 'auto'}>
            <KeyboardAwareScrollView
                style={styles.container}
                keyboardShouldPersistTaps='handled'
                bounces={false}
            >
                <CPBackButton
                    style={styles.backStyle}
                    onBackPress={navigateToBack}
                />
                <View style={styles.topView}>

                    <CPProfileImage
                        defaultImage={userSelector?.userOperation?.detail?.cover_image}
                        imagestyle={styles.widthStyle}
                        placeholder={Assets.defaultselectimage}
                        onSelectImageData={(data) => { 
                            coverImageDetail = data
                        setShouldVisible(false)
                        }}
                    />
                    <View style={styles.topImageView} >
                        <Image
                            source={Assets.cloudimage}
                            style={styles.widthStyle}
                        />

                        <CPProfileImage
                            defaultImage={userSelector?.userOperation?.detail?.profile}
                            style={styles.imageStyle}
                            placeholder={Assets.profileimage}
                            imagestyle={{ width: 90, height: 90, borderRadius: 90 }}
                            onSelectImageData={(data) => { 
                                setShouldVisible(false)
                                userProfileImageDetail = data }}
                        />

                    </View>
                </View>
                {shouldVisible && !coverImageDetail ?
                    <Text style={{ fontFamily: CPFonts.regular, fontSize: 12, color: CPColors.red, textAlign: 'center', marginTop: 10 }}>{"Please select cover image."} </Text>
                    : null}
                <View style={styles.bottomView}>
                    <CPTextInput
                        value={fullName}
                        source={Assets.accountdeselect} placeholder={'Full Name'}
                        onChangeText={onChangeFullName}
                        error={shouldVisible && validationHelper.isEmptyValidation(fullName, "Please enter full name").trim()}
                    />

                    <CPGenderSelectionComponent
                        // isPressDisable={props.route?.params?.isCurrentUser}
                        data={genderArray}
                        source={Assets.gender}
                        defaultValue={gender}
                        placeholder={"Gender"}
                        onSelectValue={setGender}
                        error={shouldVisible && !gender ? "Please select gender" : ""}
                    />

                    <Pressable
                        style={styles.termsPressable}
                        hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                        onPress={() => {
                            setlgbtq(!lgbtq);
                        }}
                    >
                        <Icon
                            type={'ionicon'}
                            name={lgbtq ? 'checkbox-outline' : 'square-outline'}
                            size={15}
                        />
                        <Text style={styles.termsCondition1}>
                            Belongs to

                            <Text style={styles.termsCondition2}>{" LGBTQ "}</Text>
                            Community
                        </Text>
                    </Pressable>

                    <CPTextInput
                        value={phoneNumber}
                        containerStyle={{ marginTop: 0 }}
                        source={Assets.call} placeholder={'Phone Number'}
                        keyboardType={'number-pad'}
                        onChangeText={onChangePhoneNumber}
                        error={shouldVisible && validationHelper.mobileValidation(phoneNumber).trim()}
                    />
                    <CPTextInput
                        value={description}
                        containerStyle={{ maxHeight: 100 }}
                        source={Assets.bio_image}
                        multiline
                        placeholder={'Bio'}
                        onChangeText={onChangeDescription}
                        error={shouldVisible && validationHelper.isEmptyValidation(description, "Please enter bio").trim()}
                    />

                    <CPThemeButton
                        title={'Save & Proceed'}
                        style={styles.btnStyle}
                        isLoading={isLoading}
                        onPress={userDetailValidation}
                    />
                </View>
            </KeyboardAwareScrollView>
        </View>
    );
};

export default UserUpdateProfileContainer;

const styles = StyleSheet.create({
    flexView: { flex: 1 },
    container: { backgroundColor: CPColors.white },
    backStyle: { position: 'absolute', left: 5, top: 50, zIndex: 1000 },
    topView: { backgroundColor: CPColors.dropdownColor, width: '100%', height: 300 },
    bottomView: { backgroundColor: CPColors.white, paddingHorizontal: 24, paddingTop: 30 },
    topImageView: { position: 'absolute', bottom: -1, right: 0, left: 0, top: 200 },
    imageStyle: { position: 'absolute', top: 10, alignSelf: 'center', borderRadius: 90 },
    btnStyle: { marginVertical: 60 },
    widthStyle: { width: '100%', height: '100%' },
    termsCondition2: {
        color: CPColors.primary,
        fontSize: 12,
        fontFamily: CPFonts.medium,
    },
    termsCondition1: {
        color: CPColors.secondary,
        fontSize: 12,
        fontFamily: CPFonts.medium,
        marginLeft: 5,
    },
    termsPressable: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 15,
    }
})