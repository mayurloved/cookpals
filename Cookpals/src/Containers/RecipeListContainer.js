import React, { useState } from 'react';
import {
  Image,
  Pressable,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import Assets from '../Assets';
import CPImageComponent from '../Components/CPImageComponent';
import CPRecipeSlider from '../Components/CPRecipeSlider';
import CPColors from '../Utils/CPColors';
import CPFonts from '../Utils/CPFonts';
import NavigationServiceManager from '../../NavigationServiceManager';
import BaseContainer from './BaseContainer';

const RecipeListContainer = (props) => {
  const entries = [
    {
      image:
        'https://hips.hearstapps.com/vidthumb/images/delish-cloud-eggs-horizontal-1536076899.jpg',
      description: '',
    },
    {
      image:
        'https://static01.nyt.com/images/2020/03/04/dining/tr-egg-curry/merlin_169211805_227972c0-43d1-4f25-9643-9568331d8adb-articleLarge.jpg',
    },
    {
      image:
        'https://www.whiskaffair.com/wp-content/uploads/2020/04/Kerala-Egg-Curry-2.jpg',
    },
    {
      image:
        'https://images-gmi-pmc.edge-generalmills.com/8dfd9c8e-1580-4508-a223-e5c15dd46d8e.jpg',
    },
  ];

  const [selectSider, setSelectedSlider] = useState(0);

  const onNavigationHome = () => {
    NavigationServiceManager.navigateToSpecificRoute('dashboard');
  }

  const exploreRecipeItemRender = ({ item, index }) => {
    return (
      <View style={styles.main}>
        <CPImageComponent style={styles.imgComponent} source={item.image} />

        <Pressable
          style={styles.cardComponent}
          onPress={() => {
            props.navigation.navigate('recipeDetail');
          }}>
          <Pressable
            onPress={() => {
              props.navigation.navigate('anotherUser', {
                isAnotherUser: true,
              });
            }}>
            <CPImageComponent
              style={styles.cpImage}
              source={
                'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80'
              }
            />
          </Pressable>
          <LinearGradient
            colors={[
              'rgba(255,255,255,0)',
              'rgba(255,255,255,0)',
              'rgba(255,255,255,0)',
              'rgba(255,255,255,0)',
              'rgba(0,0,0,0.2)',
              'rgba(0,0,0,0.5)',
              'rgba(0,0,0,0.8)',
              'rgba(0,0,0,1.0)',
            ]}
            start={{ x: 0, y: 0 }}
            end={{ x: 0, y: 1 }}
            style={styles.gradientStyle}>
            <View style={styles.recipeView}>
              <Text style={styles.nameText}>{'Lauren German'}</Text>
              <Text style={styles.bodyText}>
                Lorem Ipsum Is Simply Dummy Text Of The Ipsum Is Simply Dummy
                Text Of The
              </Text>

              <View style={styles.mapImageView}>
                {entries.map((item) => {
                  return (
                    <Image
                      style={styles.singleImg}
                      source={{ uri: item.image }}
                    />
                  );
                })}
              </View>
            </View>
          </LinearGradient>
        </Pressable>
      </View>
    );
  };

  return (
    <BaseContainer
      leftComponet={
        <Image
          style={styles.navBarImg}
          source={Assets.logo}
          resizeMode="cover"
        />
      }
      title={`Let we help us to find ${`\n`} a perfect match`}
      titleStyle={styles.titleText}>
      <View style={styles.cardView}>
        <CPRecipeSlider
          data={entries}
          componentRender={exploreRecipeItemRender}
          onBeforeSnapToItem={setSelectedSlider}
        />
        <Pressable style={{ alignSelf: 'center' }} hitSlop={{ right: 15, left: 15, bottom: 15, top: 15 }} onPress={onNavigationHome}>
          <Text style={{ textDecorationStyle: 'solid', textDecorationLine: 'underline', fontFamily: CPFonts.bold, fontSize: 16, color: CPColors.secondary }}>{"Go to home"}</Text>
        </Pressable>
      </View>
    </BaseContainer>
  );
};

const styles = StyleSheet.create({
  main: {
    flex: 1,
    marginHorizontal: 20,
    marginVertical: 45,
  },
  imgComponent: {
    height:'100%',
    width:'100%',
    borderRadius: 20
  },
  cardComponent: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    top: 0,
    justifyContent: 'space-between',
  },
  cpImage: {
    width: widthPercentageToDP('21'),
    height: widthPercentageToDP('21'),
    borderRadius: widthPercentageToDP('21'),
    position: 'relative',
    marginTop: -40,
    alignSelf: 'center',
    borderWidth: 2,
    borderColor: CPColors.white,
  },
  gradientStyle: {
    flex: 1,
    borderRadius: 20,
    justifyContent: 'flex-end',
  },
  nameText: {
    fontSize: 16,
    color: CPColors.white,
    fontFamily: CPFonts.semiBold,
    marginHorizontal: 20,
    marginVertical: 10,
  },
  bodyText: {
    fontSize: 12,
    color: CPColors.lightwhite,
    fontFamily: CPFonts.medium,
    marginHorizontal: 20,
    marginBottom: 30,
  },
  mapImageView: {
    flexDirection: 'row',
    marginRight: 10,
  },
  singleImg: {
    flex: 1,
    height: 70,
    marginLeft: 10,
    borderRadius: 15,
  },
  navBarImg: {
    marginHorizontal: 20,
    marginVertical: 10,
  },
  titleText: {
    textAlign: 'center',
    fontSize: 16,
    fontFamily: CPFonts.abril_regular,
    color: CPColors.secondary,
  },
  cardView: {
    flex: 1, paddingBottom: 20
  },
  recipeView: { paddingVertical: 30 }
});

export default RecipeListContainer;
