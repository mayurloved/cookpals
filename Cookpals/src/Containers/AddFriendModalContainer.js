import React from 'react';
import { Image, Pressable, StyleSheet, Text, View } from 'react-native';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import Assets from '../Assets';
import CPThemeButton from '../Components/CPThemeButton';
import CPColors from '../Utils/CPColors';
import CPFonts from '../Utils/CPFonts';
import BaseContainer from './BaseContainer';

const AddFriendModalContainer = (props) => {

    return (
        <BaseContainer
            isNavigationDisable
        >
            <View style={{ flex: 2 }}>
                <Image
                    source={Assets.addfriendimage}
                    style={styles.container}
                />
            </View>
            <View style={styles.container}>
                <Text style={styles.description}>
                    Lorem Ipsum is simply dummy text of the {`\n`}
                    Ipsum Is Simply Dummy Text Of The
                </Text>

                <CPThemeButton
                    title={'Add Friends'}
                    style={styles.btnStyle}
                    onPress={() => {
                        console.log("LOG    ::::: ");
                        props.navigation.navigate('addfriend')
                    }}
                />
                <Pressable style={{flexDirection:'row',alignItems:'center', alignSelf:'center', marginVertical:10}}
                onPress={()=>{
                    props.navigation.navigate('foodPreference')
                }}
                >
                        <Text style={{fontSize: 14, fontFamily: CPFonts.bold,marginVertical:10, color: CPColors.secondary}}>{"Skip"}</Text>
                        <Image style={{marginVertical:5, marginLeft:6}} resizeMode={'contain'} source={Assets.skip} />
                </Pressable>
            </View>
        </BaseContainer>
    );
};

export default AddFriendModalContainer;

const styles = StyleSheet.create({
    container: { flex: 1 },
    description: { textAlign: 'center', fontFamily: CPFonts.medium, fontSize: 12, color: CPColors.secondaryLight },
    btnStyle: { marginTop: 40, width: widthPercentageToDP("100") - 48, alignSelf: 'center' }
})