import React from 'react';
import { FlatList, Platform, StyleSheet, Text, View } from 'react-native';
import CPImageComponent from '../Components/CPImageComponent';
import CPColors from '../Utils/CPColors';
import CPFonts from '../Utils/CPFonts';
import BaseContainer from './BaseContainer';

const NotificationContainer = (props) => {

    const notificationArray = [
        {
            title: "It's a new Math",
            description: "you find a new lorem ipsum",
            image: "https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80",
            flag: true,
            time: "2m ago"
        },
        {
            title: "James Jackson send a massage",
            description: "Hi how are you...",
            image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRCe_o8_IQuNtFocDhlA6xVDAZ0CeM0fa2B3g&usqp=CAU',
            flag: false,
            time: "2h ago"
        },
        {
            title: "You are not subscribed ",
            description: "purchase our plans to explore fetures",
            image: 'https://wac-cdn.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg?cdnVersion=134',
            flag: true,
            time: "yesterday"
        },
        {
            title: "It's a new Math",
            description: "you find a new lorem ipsum",
            image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRCe_o8_IQuNtFocDhlA6xVDAZ0CeM0fa2B3g&usqp=CAU',
            flag: false,
            time: "1m ago"
        },
        {
            title: "Your subscription end in 3days",
            description: "purchase our plans to explore fetures",
            image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRCe_o8_IQuNtFocDhlA6xVDAZ0CeM0fa2B3g&usqp=CAU',
            flag: false,
            time: "2d ago"
        }
    ]

    const navigateToBack = () => {
        props.navigation.goBack()
    }
    return (
        <BaseContainer
            title={'Notification'}
            onBackPress={navigateToBack}
        >
            <FlatList
                style={styles.listContainer}
                data={notificationArray}
                renderItem={({ item }) => {
                    return (
                        <View style={[{ backgroundColor: item.flag ? CPColors.extraLightPrimary : CPColors.transparent }, styles.listView]}>
                            <CPImageComponent
                                style={styles.imageComponent}
                                source={item.image}
                            />
                            <View style={styles.subListView}>
                                <Text style={styles.titleStyle} numberOfLines={1}>{item.title}</Text>
                                <Text style={styles.descriptionStyle} numberOfLines={1}>{item.description}</Text>
                            </View>
                            <Text style={styles.timeText}>{item.time}</Text>
                        </View>
                    )
                }}
            />

        </BaseContainer>
    );
};

export default NotificationContainer;

const styles = StyleSheet.create({
    listContainer: { marginHorizontal: 10, paddingVertical: 10 },
    listView: { flex: 1, borderRadius: 10, padding: 10, flexDirection: 'row' },
    imageComponent: { width: 40, height: 40, borderRadius: 40 },
    subListView: { flex: 1, justifyContent: 'space-evenly', marginHorizontal: 15 },
    titleStyle: { fontFamily: CPFonts.semiBold, fontSize: 14, color: CPColors.secondary },
    descriptionStyle: { fontFamily: CPFonts.regular, fontSize: 14, color: CPColors.secondaryLight },
    timeText: { fontFamily: CPFonts.regular, fontSize: 10, color: CPColors.secondaryLight }
})