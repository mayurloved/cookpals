import React from 'react';
import { FlatList, KeyboardAvoidingView, Platform, StyleSheet, TextInput } from 'react-native';
import { Image, Text, View } from 'react-native';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import Assets from '../Assets';
import CPImageComponent from '../Components/CPImageComponent';
import CPColors from '../Utils/CPColors';
import CPFonts from '../Utils/CPFonts';
import BaseContainer from './BaseContainer';

const ChatContainer = (props) => {

    const chatData = [
       
        {
            message: "Hi",
            isReceiver: true
        },
        {
            message: "Hello how are you Lorem Ipsum Hello how are you Lorem Ipsum Hello how are you Lorem Ipsum",
            isReceiver: true
        },
        {
            message: "Hello how are you Lorem Ipsum Hello how are you. Lorem Ipsum Hello how are you Lorem Ipsum",
            isReceiver: false
        },
        {
            message: "Hi",
            isReceiver: true
        },
        {
            date: true,
            dateStatus: "22th January"
        }
       
    ]

    const navigateToBack = () => {
        props.navigation.goBack()
    }

    const messageInputRender = () => {
        return (
            <View style={{ flexDirection: 'row', maxHeight: 140, padding: 10, marginHorizontal: 24, marginVertical: 5, backgroundColor: CPColors.textInputColor, borderRadius: 10 }}>
                <TextInput
                    style={{ flex: 1, height: Platform.OS == 'android' ? 40 : 25, alignSelf: 'center', color: CPColors.secondary }}
                    multiline
                />
                <Image
                    style={{ width: widthPercentageToDP('9.5'), height: widthPercentageToDP('9.5'), marginLeft: 10 }}
                    source={Assets.chatsend}
                />
            </View>
        )
    }

    const messageContaint = ({ item, index }) => {
        return (
            <>
            {item.date ? 
            <View style={{backgroundColor: CPColors.primary, alignSelf:'center', margin:20, borderRadius:10}}>
                <Text style={{marginHorizontal:10, color: CPColors.lightwhite, fontFamily: CPFonts.regular, fontSize:10, marginVertical:5}}>{item.dateStatus}</Text>
            </View>
            :
            <View style={{ flex: 1, flexDirection: item.isReceiver ? 'row-reverse' : 'row', marginHorizontal: 24, marginBottom: 15 }}>
                <CPImageComponent
                    style={{ width: widthPercentageToDP("6.5"), height: widthPercentageToDP("6.5"), borderRadius: widthPercentageToDP("6.5") }}
                    source={"https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80"}
                />
                <View style={{ marginHorizontal: 10, backgroundColor: item.isReceiver ? CPColors.secondary : CPColors.textInputColor, padding: 10, borderRadius: 10, width: "70%" }}>
                    <Text style={{ fontSize: 14, fontFamily: CPFonts.medium, color: item.isReceiver ? CPColors.white : CPColors.secondary }}>{item.message}</Text>
                    <Text style={{ fontSize: 10, fontFamily: CPFonts.regular, color: item.isReceiver ? CPColors.white : CPColors.secondaryLight, marginTop: 5, alignSelf: 'flex-end' }}>{"12:00PM"}</Text>
                </View>
            </View>
            }
            </>
        )
    }

    const headerRender = () => {
        return (
            <View style={{ flex: 4, marginTop: 5, flexDirection: 'row', alignItems: 'center' }}>
                <CPImageComponent source={props.route?.params?.userData?.image}
                    style={{ width: widthPercentageToDP("11%"), height: widthPercentageToDP("11%"), borderRadius: widthPercentageToDP("11%"), borderWidth: 1 }}
                />
                <View style={{ marginHorizontal: 10, justifyContent: 'space-around' }}>
                    <Text style={{ fontSize: 16, fontFamily: CPFonts.semiBold, color: CPColors.secondaryLight }}>{props.route?.params?.userData?.name}</Text>
                    <Text style={{ fontSize: 12, fontFamily: CPFonts.medium, color: CPColors.primary }}>{
                    props.route?.params?.selectMode == 0 ? "online" : "22 Participants"
                    }</Text>
                </View>
            </View>
        )
    }

    return (
        <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' && 'padding'}
            style={styles.container}>
            <BaseContainer
                onBackPress={navigateToBack}
                leftcmpStyle={{ flex: 0, marginTop: -15 }}
                titleComponent={headerRender()}
            >

                <FlatList
                    data={chatData}
                    style={styles.listContainerStyle}
                    inverted
                    renderItem={messageContaint}
                    scrollToOverflowEnabled={true}
                    showsVerticalScrollIndicator={false}
                    
                />

                {messageInputRender()}
            </BaseContainer>
        </KeyboardAvoidingView>
    );
};

export default ChatContainer;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: CPColors.transparent,
    },
    listContainerStyle: { marginTop: 10 }
})