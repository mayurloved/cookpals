import React from 'react';
import { FlatList, View } from 'react-native';
import CPNewMemberItems from '../Components/CPNewMemberItems';
import BaseContainer from './BaseContainer';

const LikesListContainer = (props) => {

    const data = [
        {
            id: 1,
            name: "Edith Owen",
            country: "Vietnam",
            image: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80',
            follow: 1
        },
        {
            id: 2,
            name: "Steven Thornton",
            country: "Vietnam",
            image: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80',
            follow: 0
        },
        {
            id: 3,
            name: "Brent Figueroa",
            country: "Vietnam",
            image: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80',
            follow: 1
        },
        {
            id: 4,
            name: "Lores Figueroa",
            country: "Vietnam",
            image: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80',
            follow: 0
        },
        {
            id: 5,
            name: "Chris Figueroa",
            country: "Vietnam",
            image: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80',
            follow: 1
        },
        {
            id: 6,
            name: "Melvin Bridges",
            country: "Vietnam",
            image: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80',
            follow: 1
        },
    ]

    const onNavigationBack = () => {
        props.navigation.goBack()
    }

    return (
        <BaseContainer
            title={"Likes"}
            onBackPress={onNavigationBack}
       >
           <View style={{flex:1, marginHorizontal:24}}>
            <FlatList 
            data={data}
            renderItem={({item, index})=> <CPNewMemberItems item={item} onChangeStatus={()=>{}}/>}
            />
           </View>
        </BaseContainer>
    );
};

export default LikesListContainer;