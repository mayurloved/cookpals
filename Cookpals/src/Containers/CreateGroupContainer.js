import React from 'react';
import { FlatList, Platform, StyleSheet, Text, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import Assets from '../Assets';
import CPImageComponent from '../Components/CPImageComponent';
import CPProfileImage from '../Components/CPProfileImage';
import CPSelectedMemberComponent from '../Components/CPSelectedMemberComponent';
import CPTextInput from '../Components/CPTextInput';
import CPThemeButton from '../Components/CPThemeButton';
import CPColors from '../Utils/CPColors';
import CPFonts from '../Utils/CPFonts';
import BaseContainer from './BaseContainer';

const CreateGroupContainer = (props) => {

    console.log("LLLLO ::: ", props.route?.params?.groupMembers);

    const navigateToBack = () => {
        props.navigation.goBack()
    }

    return (
        <BaseContainer
            onBackPress={navigateToBack}
        >
            <View style={styles.container}>
                <CPProfileImage
                    style={styles.imageStyle}
                />

                <CPTextInput
                    source={Assets.group}
                    placeholder={"Group Name"}
                />

                <Text style={styles.headerTitle}>{'Participants' + " (" + props.route?.params?.groupMembers.length + ")"}</Text>
                <CPSelectedMemberComponent
                    membersArray={props.route?.params?.groupMembers}
                    numColumns={4}
                    style={styles.memberList}
                />
            </View>
            <CPThemeButton
                    title={"Next"}
                    style={styles.memberList}
                    onPress={() => {
                        props.navigation.popToTop()
                        props.navigation.navigate('chatlist')
                    }}
                />
        </BaseContainer>
    );
};

export default CreateGroupContainer;

const styles = StyleSheet.create({
    container: { flex: 1, paddingHorizontal: 24, paddingBottom: 10 },
    imageStyle: {
        alignSelf: 'center',
        marginVertical: 20
    },
    headerTitle: { fontSize: 16, marginTop: 40, fontFamily: CPFonts.semiBold, color: CPColors.secondary, marginBottom: 15 },
    memberList: {marginBottom:20}
})