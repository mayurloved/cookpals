import React from 'react';
import { Image, Platform, Pressable, StyleSheet, View } from 'react-native';
import { FlatList } from 'react-native';
import { Icon } from 'react-native-elements';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import Assets from '../Assets';
import CPNewChatItemList from '../Components/CPNewChatItemList';
import CPThemeButton from '../Components/CPThemeButton';
import CPColors from '../Utils/CPColors';
import BaseContainer from './BaseContainer';

const NewChatContainer = (props) => {

    var selectedMembers = []
    const newChatListArray = [
        {
            id: 1,
            name: "Edith Owen",
            country: "Vietnam",
            image: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80'
        },
        {
            id: 2,
            name: "Steven Thornton",
            country: "Vietnam",
            image: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80'
        },
        {
            id: 3,
            name: "Brent Figueroa",
            country: "Vietnam",
            image: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80'
        },
        {
            id: 4,
            name: "Lores Figueroa",
            country: "Vietnam",
            image: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80'
        },
        {
            id: 5,
            name: "Chris Figueroa",
            country: "Vietnam",
            image: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80'
        },
        {
            id: 6,
            name: "Melvin Bridges",
            country: "Vietnam",
            image: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80'
        },

    ]

    const navigationToNext = () => {

        if (props.route?.params?.selectMode == 1) {
            props.navigation.navigate('createGroup', {
                groupMembers: selectedMembers
            })
        }else{
        props.navigation.goBack()
        }
    }

    const navigateToBack = () => {
        props.navigation.goBack()
    }

    return (
        <BaseContainer
            title={props.route?.params?.selectMode == 0 ? "New Chat" : "New Group"}
            rightComponent={
                <View style={styles.flexViewStyle}>
                    <Icon
                        type={'material-icons'}
                        name={'search'}
                        color={CPColors.textInputColor}
                        style={{ marginRight: 20, backgroundColor: CPColors.dropdownColor, padding: 3, borderRadius: 10, alignSelf: 'flex-end' }}
                    />
                </View>
            }
            onBackPress={navigateToBack}
        >
            <View style={styles.container}>

                <CPNewChatItemList
                    chatGroupData={newChatListArray}
                    mode={props.route?.params?.selectMode}
                    onChangeReceiveMembers={(data) => {
                        selectedMembers = data
                    }}
                />

                <CPThemeButton
                    style={styles.btnStyle}
                    title={props.route?.params?.selectMode == 0 ? "Start Chat" : "Next"}
                    onPress={navigationToNext}
                />

            </View>
        </BaseContainer>
    );
};

export default NewChatContainer;

const styles = StyleSheet.create({
    flexViewStyle: { flex: 1 },
    container: { flex: 1, paddingBottom:10 },
    btnStyle: { width: widthPercentageToDP("85"), alignSelf: 'center' }
})