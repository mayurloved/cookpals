import React, { useEffect, useState } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import Snackbar from 'react-native-snackbar';
import { useSelector } from 'react-redux';
import Assets from '../Assets';
import CPRecipeSlider from '../Components/CPRecipeSlider';
import CPSubScriptionComponent from '../Components/CPSubScriptionComponent';
import CPColors from '../Utils/CPColors';
import { MY_SUBSCRIPTION_API, PLANS_API } from '../Utils/CPConstant';
import CPFonts from '../Utils/CPFonts';
import { getApi, postApi } from '../Utils/ServiceManager';
import BaseContainer from './BaseContainer';

const SubScriptionContainer = (props) => {

  const userSelector = useSelector((state) => state)
  const [isLoading, setIsLoading] = useState(false);

  const [subscriptionArray, setSubscriptionArray] = useState([]);

  useEffect(() => {
    if (props.route?.params?.isMySubscription) {
      mySubscriptionAction()
    } else {
      getPlansAction()
    }
  }, [])
  const navigateToBack = () => {
    props.navigation.goBack();
  };

  const mySubscriptionAction = () => {
    getApi(MY_SUBSCRIPTION_API, onSuccessMySubscription, onFailureMySubscription, userSelector.userOperation)
  }

  const onSuccessMySubscription = (response) => {
    console.log("MySubscription :::::: ", response);
    if(response.success){
    setSubscriptionArray([response.data.plan])
    }else{
      Snackbar.show({
        text: response.message,
        duration: Snackbar.LENGTH_LONG,
    });
    }
  }

  const onFailureMySubscription = (error) => {
    console.log("Error MySubscription :::::: ", error);
    Snackbar.show({
      text: error.message,
      duration: Snackbar.LENGTH_LONG,
  });
  }

  const getPlansAction = () => {
    getApi(PLANS_API, onSuccessPlanList, onFailurePlanList, userSelector.userOperation)
  }

  const onSuccessPlanList = (response) => {
    if(response.success){
    setSubscriptionArray(response.data)
    }else{
      Snackbar.show({
        text: response.message,
        duration: Snackbar.LENGTH_LONG,
    });
    }
  }

  const onFailurePlanList = (error) => {
    Snackbar.show({
      text: error.message,
      duration: Snackbar.LENGTH_LONG,
  });
  }

  const setSubscription = (id) => {
    const params = {
      plan_id: id
    }
    setIsLoading(true)
    postApi(MY_SUBSCRIPTION_API, params, onSuccessSubscription, onFailureSubscription, userSelector.userOperation)
  }

  const onSuccessSubscription = (response) => {
    setIsLoading(false)
    if(response.success){
      props.navigation.navigate('location')
    }else{
      Snackbar.show({
        text: response.message,
        duration: Snackbar.LENGTH_LONG,
    });
    }
    
  }

  const onFailureSubscription = (error) => {
    setIsLoading(false)
    Snackbar.show({
      text: error.message,
      duration: Snackbar.LENGTH_LONG,
  });
  }

  const onSubscriptionAction = (id) => {
    if (!props.route?.params?.isMySubscription) {
      setSubscription(id)
    }
  }

  return (
    <BaseContainer
      isNavigationDisable={props.route?.params?.isMySubscription ? false : true}
      title={'My Subscription'}
      isLoading={isLoading}
      onBackPress={navigateToBack}>
      <View style={styles.main}>
        {props.route?.params?.isMySubscription ? null : (
          <View
            style={styles.subScriptionView}>
            <Image source={Assets.logo} />
            <View style={{}}>
              <View style={styles.subView} />
              <Text style={styles.textStyle}>{'Best Plans For You'}</Text>
            </View>
          </View>
        )}

        {/* <CPSubScriptionComponent
          onPress={() => {
            props.navigation.navigate('adduserdetail');
          }}
        /> */}

        <CPRecipeSlider
          data={subscriptionArray}
          componentRender={({ item }) => <CPSubScriptionComponent
            item={item}
            isLoading={isLoading}
            onPress={onSubscriptionAction}
            color={CPColors.secondary}
            title={'Repurchased'}
          />
          }
          layoutCardOffset={0}
          addonsComponentRender={
            <View style={styles.backgroundViewStyle} />
          }
        // onBeforeSnapToItem={setSelectedSlider}
        // onSnapToItem={setSelectedSlider}
        // addMargin={40}
        />
      </View>
    </BaseContainer>
  );
};

export default SubScriptionContainer;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    // marginHorizontal: 20,
    marginVertical: 20,
  },
  backgroundViewStyle: {
    position: 'absolute',
    alignSelf: 'center',
    top: 70, bottom: 30, left: 20, right: 20,
    // width: widthPercentageToDP("90%"),
    // height: heightPercentageToDP("60%"),
    backgroundColor: CPColors.secondaryLight,
    borderRadius: 30
  },
  subView: {
    flexDirection: 'row',
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  textStyle: {
    fontSize: 26,
    fontFamily: CPFonts.bold,
    color: CPColors.secondary,
  },
  subScriptionView: {
    marginHorizontal: 20,
    flexDirection: 'row',
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'space-between',
  }
});
